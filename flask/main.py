import os
from flask import Flask, Response, request, abort
from flask_mail import Mail, Message
from recaptcha import is_recaptcha_passed

server = Flask('flask', root_path='/flask/')
server.config.from_object('config.Config')

#: Recipient should be set to valid email address that will receive the mail - your email address probably.
RECIPIENTS = [os.getenv('RECIPIENT', 'your.email@example.com')]
#: Maximum length allowed for message field, default - 300.
MESSAGE_MAX_LENGTH = int(os.getenv('MESSAGE_MAX_LENGTH', '300'))
#: Maximum length allowed for name field, default - 64.
NAME_MAX_LENGTH = int(os.getenv('NAME_MAX_LENGTH', '64'))
#: Maximum length allowed for email field, default - 254.
EMAIL_MAX_LENGTH = int(os.getenv('EMAIL_MAX_LENGTH', '254'))
#: Custom email subject can be configured via environment variable.
SUBJECT = os.getenv('SUBJECT', 'Personal Website Contact Form Message')
#: Flag for cases when ReCAPTCHA is not used, default - 'false', value 'true' will turn it off.
DISABLE_RECAPTCHA = os.getenv('DISABLE_RECAPTCHA', 'false') == 'true'

#: Initializing Flask-Mail instance to send mail.
MAIL = Mail(server)

def send_mail(body_html: str) -> None:
    """
    Sends HTML mail to one recipient.

    :param body_html: HTML body.
    """
    message = Message(SUBJECT, recipients=RECIPIENTS)
    message.html = body_html
    MAIL.send(message)

@server.route("/", methods=["POST"])
def mail_endpoint() -> Response:
    """
    This endpoint accepts forms for email sending and sends it via SMTP service installed on host.
    You should POST data with "application/x-www-form-urlencoded" type.
    Form should contain 4 fields: g-recaptcha-response, message, name and email.
    """
    # First thing that is checked - if ReCAPTCHA check was made and if user passed it (or ignore it if DISABLE_RECAPTCHA flag is true).
    if "g-recaptcha-response" in request.form or DISABLE_RECAPTCHA:
        if not DISABLE_RECAPTCHA and not is_recaptcha_passed(request.form["g-recaptcha-response"]):
            abort(401) # Unauthorized status code - you can signal user that he didn't passed the check.

        # After that we check if all other fields are in request and don't exceed their length limits.
        if "message" in request.form and "name" in request.form and "email" in request.form:
            if len(request.form["message"]) > MESSAGE_MAX_LENGTH or len(request.form["name"]) > NAME_MAX_LENGTH or len(request.form["email"]) > EMAIL_MAX_LENGTH:
                abort(403) # Forbidden status code - you can signal user that there are length limits which he ignored.

            message_html = "<h1>Message from contact form!</h1>\
                            <h3>From: <u>" + request.form["name"] + "</u></h3>\
                            <h3>Reply To: <u>" + request.form["email"] + "</u></h3>\
                            </br><b>Message:</b></br></br><pre>" + request.form["message"] + "</pre>"

            send_mail(message_html)

    abort(400) # Bad Request status code - somebody tried to send random data to endpoint or form is misconfigured.
