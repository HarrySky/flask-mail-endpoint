import os
import json
from urllib.request import Request, urlopen
from urllib.parse import urlencode

#: Getting ReCAPTCHA secret key from environment variable.
RECAPTCHA_SECRET_KEY = os.getenv('RECAPTCHA_SECRET_KEY', 'your-secret-key')

def is_recaptcha_passed(recaptcha_response_field: str) -> bool:
    """Checks if ReCAPTCHA response is from valid human being via Google ReCAPTCHA API."""

    # Encoding our secret code and response from user
    params = urlencode({
        'secret': RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response_field
    }).encode("utf-8")

    # Creating request to Google ReCAPTCHA API
    request = Request(
        url='https://www.google.com/recaptcha/api/siteverify',
        data=params,
        headers={
            'Content-type': 'application/x-www-form-urlencoded',
            'User-agent': 'ReCAPTCHA Python'
        }
    )

    # Getting response from Google ReCAPTCHA API
    httpresponse = urlopen(request)
    data = httpresponse.read()
    encoding = httpresponse.info().get_content_charset('utf-8')
    return_values = json.loads(data.decode(encoding))
    httpresponse.close()

    # Checking if user passed ReCAPTCHA
    if return_values['success']:
        return True

    return False
